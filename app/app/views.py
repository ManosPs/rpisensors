from os import truncate
from app import app
# from app import relay
from flask import render_template, request, redirect, make_response, jsonify



@app.route("/")
def index():
    
    title = "Home"

    #print(app.config)
    
    return render_template("public/index.html", title=title)
    # return render_template("startbootstrap-sb-admin/dist/index.html")

@app.route("/about")
def about():

    title = "About"
    
    return render_template("public/about.html", title=title)

@app.route("/sign-up", methods=["GET", "POST"])
def sing_up():
    title = "Sign-up"

    if request.method == "POST":

        req = request.form

        username = req["username"]
        email = req.get("email")
        password = request.form["password"]

        print( f"Username: {username}, Email: {email}, Password: {password}" )

        return redirect(request.url)
    
    return render_template("public/sign_up.html", title=title)

users = {
    "mitsuhiko": {
        "name": "Armin Ronacher",
        "bio": "Creator of the Flask framework",
        "twitter_handle": "@mitsuhiko"
    },
    "gvanrossum": {
        "name": "Guido Van Rossum",
        "bio": "Creator of the Python programming language",
        "twitter_handle": "@gvanrossum"
    },
    "elonmusk": {
        "name": "Elon Musk",
        "bio": "technology entrepreneur, investor, and engineer",
        "twitter_handle": "@elonmusk"
    },
    "tom": {

    }
}


@app.route("/profile/<username>")
def profile(username):

    user = None

    if username in users:
        user = users[username]
        title = username + " Profile"
    else:
        username = "non-existent"
        title = username + " User"
    

    return render_template("public/profile.html", title=title, username=username, user=user)


@app.route("/multiple/<foo>/<bar>/<baz>")
def multiple(foo, bar, baz):

    print(f"foo is {foo}")
    print(f"bar is {bar}")
    print(f"baz is {baz}")


    return f"foo is {foo}, bar is {bar}, baz is {baz}"


@app.route("/temp")
def temp():

    title = "Temperature"
    
    return render_template("public/temp.html", title=title)

# import gpiozero
# import sys
# RELAY_PIN = 21
# relay = gpiozero.OutputDevice(RELAY_PIN, active_high=False, initial_value=False)

# def set_relay(status):

#     # global relay
#     if status:
#         print("Setting relay: ON")
#         relay.on()
#         return
#     else:
#         print("Setting relay: OFF")
#         relay.off()
#         return
relayStatus = False

@app.route("/relay", methods=["GET", "POST"])
def relay():

    # RELAY_PIN = 21
    # relay = gpiozero.OutputDevice(RELAY_PIN, active_high=False, initial_value=False)
    # GPIO.setup(RELAIS_PIN, GPIO.OUT)

    title = "Relay"
    global relayStatus
       
    return render_template("public/relay.html", title=title, relayStatus=relayStatus)

import RPi.GPIO as GPIO
# import gpiozero
# import RPi.GPIO as GPIO
# RELAY_PIN = 21
# relay = gpiozero.OutputDevice(RELAY_PIN, active_high=True, initial_value=False)

@app.route("/relay/toggle", methods=["POST"])
def relay_toggle():
    global relayStatus
    GPIO.setmode(GPIO.BCM)
    RELAY_PIN = 21
    # relayss = gpiozero.OutputDevice(RELAY_PIN, active_high=True, initial_value=False)
   
    GPIO.setup(RELAY_PIN, GPIO.OUT)
    GPIO.output(RELAY_PIN, False)
   
    # GPIO.setmode(GPIO.BOARD)
    # GPIO.setup(RELAY_PIN, GPIO.OUT)
    # print(relay.value)  
    req = request.get_json()
    print (req["choice"]) 
    # print (type(req))
    status = req["choice"]
    # relay.set_relay(status)
    res = make_response(jsonify(req), 200)

    if status == True:
        # GPIO.output(RELAY_PIN,GPIO.HIGH)
        # relayss.on()
        print("GPIO on")
        GPIO.output(RELAY_PIN, True) # NO is now connected through
        # print(relay.value)
        relayStatus = True
    else:
        # relayss.off()
        print("GPIO off")
        GPIO.output(RELAY_PIN, False) # NC is now connected through
        # GPIO.output(RELAY_PIN,GPIO.LOW)
        relayStatus = False

    # relay.manual_switch(status)
    # if status==True:
    #     relay.on()
    #     # stop_run = False
    #     return res
    # else:
    #     relay.off()
    #     # stop_run = True
    return res
    # return res, relay.manual_run(status)


@app.route("/video")
def video():

    title = "Video"
    
    return render_template("public/video.html", title=title)


@app.route("/ph")
def ph():

    title = "PH"
    
    return render_template("public/ph.html", title=title)


@app.route("/hum")
def hum():

    title = "Humidity"
    
    return render_template("public/hum.html", title=title)