from flask import Flask

import sqlite3

app = Flask(__name__)

from app import views
from app import admin_views
from app import configs
from app import logDS18B20
# from app import relay
