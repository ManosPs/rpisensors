from app import app
from app import logDS18B20

from flask import render_template, jsonify, request, g, make_response, json, Response

import time


@app.route("/admin/profile")
def admin_profile():

    title = "Admin Profile"
    
    return render_template("admin/profile.html", title=title)

@app.route("/admin/dashboard")
def admin_dashboard():

    title = "Dashboard"

    return render_template("admin/dashboard.html", title=title)

@app.route("/admin/get/temp")
def get_temp():
    temp = logDS18B20.getDS18B20data()
    res = make_response(jsonify(temp), 200)
    return res

@app.route("/admin/get/lastData")
def lastData():
    dictLastData = logDS18B20.displayLastData()
    #print(timeL, tempL)
    res = make_response(json.dumps(dictLastData), 200)
    return res

@app.route("/admin/get/allData")
def allData():
    times = []
    temps = []
    times, temps = logDS18B20.displayData()
    res = make_response(jsonify(times,temps), 200)
    return res


stop_run = False
sampleFreq = 20 # time in seconds

def log_data():
    global stop_run
    global sampleFreq
    while not stop_run:
        logDS18B20.logData()
        logDS18B20.displayLastData()
        # print(stop_run)
        print (f"Samples Frequence = {sampleFreq} second")
        time.sleep(sampleFreq)


from threading import Thread

def manual_run():
    t = Thread(target=log_data)
    t.start()
    return "Processing"

@app.route ("/admin/samples/freq", methods=["POST"])
def samples_freq():
    global sampleFreq
    req = request.get_json()
    freqMS = int(req['choice'])
    sampleFreq = int(freqMS/1000)
    print(f"Sample Frequence adjusted to: {sampleFreq} second")

    # print(req['choice'])
    # print(type(choice))

    res = make_response(jsonify(req), 200)
    return res

@app.route("/admin/toggle/sensors", methods=["POST"])
def toggle_sensors():
    global stop_run
    req = request.get_json()
    print(req)
    res = make_response(jsonify(req), 200)
    if req:
        stop_run = False
        return (res, manual_run())
    else:
        stop_run = True
        return res
