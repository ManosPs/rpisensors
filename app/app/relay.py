import gpiozero



RELAY_PIN = 21
relay = gpiozero.OutputDevice(RELAY_PIN, active_high=True, initial_value=False)
print(relay.value)
def on():
    print("Setting relay: ON")
    relay.on()
                
def off():

    print("Setting relay: OFF")
    relay.off()
    print(relay.value)    
from threading import Thread

def manual_switch(status):
    print ("manual_run STATUS: ",status)
    if status == True:
        k = Thread(target=on())
        k.start()
        return "Processing on"
    else:
        p = Thread(target=off())
        p.start()
        return "Processing off"