import os
import glob
import time
import gpiozero
import sqlite3
from datetime import datetime


dbname = 'app/data/sensorData.db'
sampleFreq = 1*20 # time in seconds

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'

def date_converter(time):
	str(time)

	return time
	

def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

# get data from DS18B20 sensor
def getDS18B20data():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
      #  temp_f = temp_c * 9.0 / 5.0 + 32.0
        return temp_c

# log sensor data on database
def logData():
	temp_c = getDS18B20data()
	conn=sqlite3.connect(dbname)
	curs=conn.cursor()
	curs.execute("INSERT INTO DS18B20_data values(datetime('now'), (?))", [temp_c])
	conn.commit()
	conn.close()

# display database data
def displayData():
	conn=sqlite3.connect(dbname)
	curs=conn.cursor()
	#print ("\nEntire database contents:\n")
	times=[]
	temps=[]
	for row in curs.execute("SELECT * FROM DS18B20_data DESC LIMIT 10"):		
		#print (row)
		time = date_converter(row[0])
		times.append(time)
		temps.append(row[1])
	conn.close()
	return times,temps

# display last data from database
def displayLastData():
	# temp = getDS18B20data()
	# logData (temp)
	conn=sqlite3.connect(dbname)
	curs=conn.cursor()
	#print ("\nLast database content:\n")
	lastRow = []
	curs.execute("SELECT * FROM DS18B20_data ORDER BY timestamp DESC LIMIT 1")
	lastRow = curs.fetchone()
	# print (lastRow)
	
	time = date_converter(lastRow[0])
	temp = lastRow[1]
	
	dictLastRow = dict([("x", time), ("y", temp)])

	print (dictLastRow)
	conn.close()
	return dictLastRow

# main function
import sys
con = False
def main(run):
	con = run

	print("con = ",con)
	while con :
		print ("sampleFreq = ", sampleFreq)
		print("con_in = ",con)
		logData ()
		displayLastData()
		time.sleep(sampleFreq)
	else: return
		
		

# execute program
# main()