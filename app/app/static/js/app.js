console.log("Hello from app.js");
let t = new Date();
console.log(t);

function submit_message() {

  var name = document.getElementById("name");
  var message = document.getElementById("message");

  var entry = {
    name: name.value,
    message: message.value
  };

  fetch(`${window.origin}/configs/ajax/create-entry`, {
    method: "POST",
    credentials: "include",
    body: JSON.stringify(entry),
    cache: "no-cache",
    headers: new Headers({
      "content-type": "application/json"
    })
  })
    .then(function (response) {
      if (response.status !== 200) {
        console.log(`Looks like there was a problem. Status code: ${response.status}`);
        return;
      }
      response.json().then(function (data) {
        console.log(data);
        document.getElementById("namem").innerHTML = data.name + " says:";
        document.getElementById("messagem").innerHTML = data.message;
      });
    })
    .catch(function (error) {
      console.log("Fetch error: " + error);
    });

}
var to;
function get_temp() {

  fetch(`${window.origin}/admin/get/temp`)

    .then(function (response) {
      if (response.status !== 200) {
        console.log(`Looks like there was a problem. Status code: ${response.status}`);
        return;
      }
      response.json().then(function (data) {
        clearTimeout(to);
        console.log(data);
        document.getElementById("temp").innerHTML = data + " °C";
        to = setTimeout(function () { document.getElementById("temp").innerHTML = "Get newest Temperature!"; }, 5000);

      });
    })
    .catch(function (error) {
      console.log("Fetch error: " + error);
    });

}
var interval = 20000;
var intervalID = setInterval(get_last_data, interval);
var loc = window.location.href
console.log(loc)
if (loc !== `${window.origin}/admin/dashboard`) {
  clearInterval(intervalID);
}

// console.log(typeof temp);
var date = [];
var temp = [];
function get_last_data() {

  fetch(`${window.origin}/admin/get/lastData`, {
    method: "GET",
    credentials: "include",
    cache: "no-cache",
    headers: new Headers({
      "content-type": "application/json"
    })
  })

    .then(function (response) {
      if (response.status !== 200) {
        console.log(`Looks like there was a problem. Status code: ${response.status}`);
        return;
      }
      response.json().then(function (res) {
        console.log(res);
        console.log("res.x = ", res.x);

        var l = date[date.length - 1];
        console.log("last item of date = ", l);

        // date = res.x;

        // temp = res.y;
        // console.log(date);
        // console.log(temp);
        //  console.log(typeof temp);

        myChart.data.labels.push(res.x);
        myChart.data.datasets.forEach((dataset) => {
          dataset.data.push(res.y);
        });
        if (res.x !== l) {
          myChart.update();
        } else {
          clearInterval(intervalID);
        }

      });

      var ctx = document.getElementById('myChart');
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: date,
          datasets: [{
            label: 'Temperature',
            data: temp,
            backgroundColor: [
              'rgba(102, 255, 153, 1)'
            ],
            borderColor: [
              'rgba(255, 0, 0, 1)'
            ],
            borderWidth: 1
          }]
        },
        options: {
          events: ['click'],
          onClick: null,
          tooltips: {
            intersect: true,
            mode: 'point'
          }
        }
      });
    })
    .catch(function (error) {
      console.log("Fetch error: " + error);
    });

}
// var togSen = $('#toggle-sensors').prop('checked');
// console.log('togSen: ' + togSen);
// allCookies = document.cookie;
// console.log('cookies = ', allCookies);

function checkToggleBtn(){ 
  let locStor = localStorage.getItem('toggle')
  // console.log('locStor = ',locStor)
  if (locStor === 'true'){
    $('#toggle-sensors').bootstrapToggle('on');
  } else {
    $('#toggle-sensors').bootstrapToggle('off');
  }
  
}
$('#toggle-sensors').change(function () {
  var toggle = $('#toggle-sensors').prop('checked');
  // console.log('Toggle: ' + toggle);
  // toggle == togSen
if (toggle === true){
  localStorage.setItem('toggle', 'true')
}else {
  localStorage.setItem('toggle', 'false')

}

  fetch(`${window.origin}/admin/toggle/sensors`, {
    method: "POST",
    credentials: "include",
    body: JSON.stringify(toggle),
    cache: "no-cache",
    headers: new Headers({
      "content-type": "application/json"
    })
  })
    .then(function (response) {
      if (response.status !== 200) {
        console.log(`Looks like there was a problem. Status code: ${response.status}`);
        return;
      }
      response.json().then(function (data) {
        console.log("sensors = ",data)
        // console.log(typeof(data))
        if (data === true){
          console.log(interval)
          
          intervalID = setInterval(get_last_data, interval);
        } 
      });
    })
    .catch(function (error) {
      console.log("Fetch error: " + error);
    });
});



function submit_samplesFreq() {

  var choice = document.getElementById("inputFreq");

  var entry = {
    choice: inputFreq.value
  };

  fetch(`${window.origin}/admin/samples/freq`, {
    method: "POST",
    credentials: "include",
    body: JSON.stringify(entry),
    cache: "no-cache",
    headers: new Headers({
      "content-type": "application/json"
    })
  })
    .then(function (response) {
      if (response.status !== 200) {
        console.log(`Looks like there was a problem. Status code: ${response.status}`);
        return;
      }
      response.json().then(function (data) {

        console.log(Number(data.choice));
        interval = Number(data.choice) + 100 ;
        
        console.log(interval);
        // intervalID = setInterval(get_last_data, interval);
        // document.getElementById("sampleFreq").innerHTML = data.choice ;

      });
    })
    .catch(function (error) {
      console.log("Fetch error: " + error);
    });
}

function enableButton()
{
    var selectelem = document.getElementById('inputFreq');
    var btnelem = document.getElementById('btnFreq');
    btnelem.disabled = !selectelem.value;
}



function showTime(){
  var date = new Date();
  var h = date.getHours(); // 0 - 23
  var m = date.getMinutes(); // 0 - 59
  var s = date.getSeconds(); // 0 - 59
  var session = "AM";
  
  if(h == 0){
      h = 12;
  }
  
  if(h > 12){
      h = h - 12;
      session = "PM";
  }
  
  h = (h < 10) ? "0" + h : h;
  m = (m < 10) ? "0" + m : m;
  s = (s < 10) ? "0" + s : s;
  
  var time = h + ":" + m + ":" + s + " " + session;
  document.getElementById("MyClockDisplay").innerText = time;
  document.getElementById("MyClockDisplay").textContent = time;
  
  setTimeout(showTime, 1000);
  
}

showTime();


let lsRelay = localStorage.getItem('toggleRelay')

function submit_relayStatus() {

  var relayS = document.getElementById("relayS");
  console.log(relayS.checked)

  var entry = {
    choice: relayS.checked
  };

  fetch(`${window.origin}/relay/toggle`, {
    method: "POST",
    credentials: "include",
    body: JSON.stringify(entry),
    cache: "no-cache",
    headers: new Headers({
      "content-type": "application/json"
    })
  })
    .then(function (response) {
      if (response.status !== 200) {
        console.log(`Looks like there was a problem. Status code: ${response.status}`);
        return;
      }
      response.json().then(function (data) {
        lsRelay = localStorage.setItem('toggleRelay', data.choice)
        console.log(data.choice);

        // interval = Number(data.choice) + 100 ;
        
        // console.log(interval);
        // intervalID = setInterval(get_last_data, interval);
        // document.getElementById("sampleFreq").innerHTML = data.choice ;

      });
    })
    .catch(function (error) {
      console.log("Fetch error: " + error);
    });
}


if (loc === `${window.origin}/relay`){
  // var status = document.getElementById("relaystatus").getAttribute('value');
  // console.log(status)
  if ( lsRelay === "true" ) {
    console.log("status", lsRelay)
    document.getElementById("relayS").checked = true;
  }
  
  // submit_relayStatus()

  $('#relayS').change(function () {
    var toggle = $('#relayS').prop('checked');
    console.log('Toggle RELAY: ' + toggle);
    submit_relayStatus()

  })

}




/* function get_data() {

  fetch(`${window.origin}/admin/get/allData`)

    .then(function (response) {
      if (response.status !== 200) {
        console.log(`Looks like there was a problem. Status code: ${response.status}`);
        return;
      }
      response.json().then(function (res) {
        // console.log(res);
        let times = res[0];
        let temps = res[1];

        // console.log(times);
        // console.log(temps);
        // document.getElementById("temp").innerHTML = data + " °C";
        return res;
      });
    })
    .catch(function (error) {
      console.log("Fetch error: " + error);
    });

}

window.onload = function () {


   fetch(`${window.origin}/admin/get/allData`)
    .then(function (response) {
      if (response.status !== 200) {
        console.log(`Looks like there was a problem. Status code: ${response.status}`);
        return;
      }
      response.json().then(function (res) {
        // console.log(res);
        let timesS = [];
        let tempsS = [];
        // console.log(res);
        // console.log(temps);

        for (i in res[0]) {
          timesS.push(res[0][i]);
        }
        for (i in res[1]) {
          tempsS.push(res[1][i]);
        }

        times = JSON. stringify(timesS)
        temps = JSON. stringify(tempsS)
        console.log(res);
        console.log(typeof(temps));






      var ctx = document.getElementById('myChart');
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: [data[0]],
          datasets: [{
            data: [data[1]],
            label: "Temperatur [°C]",
            borderColor: "#8e5ea2",
            fill: false
          }
          ]
        },
        options: {

          title: {
            display: true,
            text: 'Historical Data of Temperature'
          },
          scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }

        }
      });
    });

  });
}
*/


