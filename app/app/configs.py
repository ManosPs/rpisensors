from app import app

from flask import render_template, request, jsonify, make_response

from datetime import datetime


@app.route("/configs/configs")
def configs_page():

    title = "Configurations"

    return render_template("configs/configs.html", title=title)


@app.route("/configs/jinja")
def jinja():

    title = "Jinja Features"

    # Strings
    my_name = "Julian"

    # Integers
    my_age = 30

    # Lists
    langs = ["Python", "JavaScript", "Bash", "Ruby", "C", "Rust"]

    # Dictionaries
    friends = {
        "Tony": 43,
        "Cody": 28,
        "Amy": 26,
        "Clarissa": 23,
        "Wendell": 39
    }

    # Tuples
    colors = ("Red", "Blue")

    # Booleans
    cool = True

    # Classes
    class GitRemote:
        def __init__(self, name, description, domain):
            self.name = name
            self.description = description
            self.domain = domain

        def clone(self, repo):
            return f"Cloning into {repo}"

    my_remote = GitRemote(
        name="Learning Flask",
        description="Learn the Flask web framework for Python",
        domain="https://github.com/Julian-Nash/learning-flask.git"
    )

    # Functions
    def repeat(x, qty=1):
        return x * qty

    date = datetime.utcnow()

    my_html = "<h1>This is some HTML</h1>"
    suspicious = "<script>alert('NEVER TRUST USER INPUT!')</script>"



    return render_template("configs/jinja.html", title=title, my_name=my_name,
        my_age=my_age, langs=langs, friends=friends, cool=cool, colors=colors,
        GitRemote=GitRemote, my_remote=my_remote, repeat=repeat, date=date,
        my_html=my_html, suspicious=suspicious
    )


@app.template_filter("clean_date")
def clean_date(dt):

    return dt.strftime("%d %b %Y %H:%M:%S")


@app.route("/configs/ajax", methods=["GET", "POST"])
def ajax():

    title = "AJAX"


    return render_template("configs/ajax.html", title=title)


@app.route("/configs/ajax/create-entry", methods=["POST"])
def create_entry():

    req = request.get_json()

    print(req)

    res = make_response(jsonify(req), 200)
    
    return res






""" @app.route("/configs/json", methods=["POST"])
def json():
    # Validate the request body contains JSON
    if request.is_json:

        # Parse the JSON into a Python dictionary
        req = request.get_json()

        # Print the dictionary
        print(req)

        # Return a string along with an HTTP status code
        return "JSON received!", 200

    else:

        # The request body wasn't JSON so return a 400 HTTP status code
        return "Request was not JSON", 400 """

#cURL request (POST JSON):
#curl --header "Content-Type: application/json" --request POST 
# --data '{"name":"Julian","message":"Posting JSON data to Flask!"}' 
# http://127.0.0.1:5000/json

""" @app.route("/json", methods=["POST"])
def json_example():

    if request.is_json:

        req = request.get_json()

        response_body = {
            "message": "JSON received!",
            "sender": req.get("name")
        }

        res = make_response(jsonify(response_body), 200)

        return res

    else:

        return make_response(jsonify({"message": "Request body must be JSON"}), 400) """
